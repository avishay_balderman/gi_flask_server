from flask import Flask, request, abort, send_from_directory
from flask_restful import Api, Resource, reqparse
from org.gi.config import config
import argparse

import pymongo
from pymongo import IndexModel
import os
from org.gi.server import validations as v
from org.gi.server.authorization import requires_auth
import org.gi.server.authorization as auth
from org.gi.server import utils as u
from org.gi.server import location as l

from sys import platform as _platform
import logging
import uuid


app = Flask(__name__, static_url_path='')
api = Api(app)
FORMAT = '%(asctime)-15s  %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('werkzeug')
log.setLevel(logging.DEBUG)
log.addHandler(logging.StreamHandler())


def _get_static_folder(_type):
    if _platform in ['darwin', 'linux2', 'linux']:
        path_array = os.getcwd().split('/')
        path_array = path_array[:len(path_array) - 3]
        path_array.append('static')
        path_array.append(_type)
        return '/'.join(path_array)
    else:
        return '..\..\..%sstatic%s%s' % (os.path.sep, os.path.sep, _type)


@app.route('/html/<path:path>')
def send_html(path):
    return send_from_directory(_get_static_folder('html'), path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory(_get_static_folder('css'), path)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory(_get_static_folder('js'), path)


@app.route('/api/ping')
def ping():
    return 'Pong', u.HTTP_OK


class Case(Resource):
    def _handle_geo_location(self, case):
        fields = ['destination_address', 'address']
        for task in case['tasks']:
            for field in fields:
                address = task.get(field)
                if address:
                    geo = l.get_lat_lng(address)
                    address['geo'] = geo

    @requires_auth
    def get(self, case_id):
        try:
            case = db.cases.find_one({'_id': u.to_object_id(case_id)})
            u.handle_id(case)
        except Exception as e:
            abort(u.HTTP_NOT_FOUND, str(e))
        return case, u.HTTP_OK

    @requires_auth
    def delete(self, case_id):
        try:
            result = db.cases.delete_one({'_id': u.to_object_id(case_id)})
            if result.deleted_count != 1:
                raise Exception('One case should be deleted but %d cases were deleted' % result.deleted_count)
        except Exception as e:
            return str(e), u.HTTP_NOT_FOUND
        return '', u.HTTP_NO_CONTENT

    @requires_auth
    def put(self, case_id):
        case = db.cases.find_one({'_id': u.to_object_id(case_id)})
        if not case:
            return 'Could not find a user with id %s' % case_id, u.HTTP_NOT_FOUND
        else:
            faults = []
            v.case_put_validate(case, request.json, faults)
            if faults:
                log.debug("Failed to update a case. Faults: %s", str(faults))
                return {'errors': faults}, u.HTTP_BAD_INPUT
            try:
                incoming_case = request.json
                self._handle_geo_location(incoming_case)
                diff = u.diff_dict(case, request.json)
                result = db.cases.update_one({"_id": u.to_object_id(case_id)}, {'$set': incoming_case})
                if result.modified_count != 1:
                    msg = '%d cases where modified. One case only should be modified. Case details: id: %s data for update %s' % (
                    result.modified_count, case_id, str(incoming_case))
                    raise Exception(msg)
            except Exception as e:
                log.debug("Failed to update a case. Exception:: %s", str(e))
                abort(u.HTTP_BAD_INPUT, str(e))
            return '', u.HTTP_NO_CONTENT

    @requires_auth
    def post(self):
        faults = []
        v.case_post_validate(request.json, db, faults)
        if faults:
            log.debug("Failed to create a case. Faults: %s", str(faults))
            return {'errors': faults}, u.HTTP_BAD_INPUT
        try:
            case = request.json
            for task in case['tasks']:
                task['id'] = str(uuid.uuid4())
            self._handle_geo_location(case)
            _id = db.cases.insert(case)
        except Exception as e:
            abort(u.HTTP_BAD_INPUT, str(e))
        created = db.cases.find_one({'_id': u.to_object_id(_id)})
        if not created:
            msg = 'Failed to find a newly created case. Using id %s' % u.to_object_id(_id)
            log.debug(msg)
            abort(u.HTTP_SERVER_ERROR, msg)
        u.handle_id(created)
        return created, u.HTTP_CREATED


class CaseList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        super(CaseList, self).__init__()

    @requires_auth
    def get(self):
        try:
            _filter, projection, sort, page_size_str, page_number_str = u.get_fields_projection_and_filter(request)
            cases = db.cases.find(projection=projection, filter=_filter)
            cases = u.handle_sort_and_paging(cases, sort, page_size_str, page_number_str)
            if cases and sort:
                cases = cases.sort(sort)
        except Exception as e:
            abort(u.HTTP_SERVER_ERROR, str(e))
        return u.make_list(cases), u.HTTP_OK


class Login(Resource):

    def get(self):
        _args = u.query_string_to_dict(request)
        if not args:
            return 'Request in has no query arguments', u.HTTP_BAD_INPUT
        user_name = _args.get('user_name')
        if not user_name:
            return 'user_name is None', u.HTTP_BAD_INPUT
        password = _args.get('password')
        if not password:
            return 'password is None', u.HTTP_BAD_INPUT
        return ('', u.HTTP_OK) if auth.check_auth(user_name, password) else ('', u.HTTP_UNAUTHORIZED)


class UserList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        super(UserList, self).__init__()

    # @requires_roles(auth.ROLE_ADMIN)
    @requires_auth
    def get(self):
        try:
            _filter, projection, sort, page_size_str, page_number_str = u.get_fields_projection_and_filter(request)
            users = db.users.find(projection=projection, filter=_filter)
            users = u.handle_sort_and_paging(users, sort, page_size_str, page_number_str)
        except Exception as e:
            abort(u.HTTP_SERVER_ERROR, str(e))
        return u.make_list(users), u.HTTP_OK


class User(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        super(User, self).__init__()

    def _pad_with_roles(self, payload):
        if payload and isinstance(payload, dict) and not payload.get('role'):
            payload['role'] = auth.USER

    @requires_auth
    def get(self, user_id):
        try:
            user = db.users.find_one({'_id': u.to_object_id(user_id)})
            u.handle_id(user)
        except Exception as e:
            abort(u.HTTP_NOT_FOUND, str(e))
        return user, u.HTTP_OK

    @requires_auth
    def delete(self, user_id):
        try:
            result = db.users.delete_one({'_id': u.to_object_id(user_id)})
            if result.deleted_count != 1:
                raise Exception('One user should be deleted but %d users were deleted' % result.deleted_count)
        except Exception as e:
            return str(e), u.HTTP_NOT_FOUND
        return '', u.HTTP_NO_CONTENT

    @requires_auth
    def post(self):
        faults = []
        payload = request.json
        self._pad_with_roles(payload)
        v.post_validate(payload, v.USER_META, faults)
        if faults:
            log.debug("Failed to create a user. Faults: %s", str(faults))
            return {'errors': faults}, u.HTTP_BAD_INPUT
        try:
            user = request.json
            user['password'] = auth.hash_password(user['password'])
            id = db.users.insert(user)
        except Exception as e:
            abort(u.HTTP_BAD_INPUT, str(e))
        created = db.users.find_one({'_id': u.to_object_id(id)})
        u.handle_id(created)
        return created, u.HTTP_CREATED

    @requires_auth
    def put(self, user_id):
        user = db.users.find_one({'_id': u.to_object_id(user_id)})
        if not user:
            return 'Could not find a user with id %s' % user_id, u.HTTP_NOT_FOUND
        else:
            faults = []
            payload = request.json
            self._pad_with_roles(payload)
            v.put_validate(payload, v.USER_META, faults)
            if faults:
                return {'errors': faults}, u.HTTP_BAD_INPUT
            try:
                result = db.users.update_one({'_id': u.to_object_id(user_id)}, request.json)
                if result.modified_count != 1:
                    msg = '%d users where modified. One user only should be modified.' % result.modified_count
                    raise Exception(msg)
            except Exception as e:
                abort(u.HTTP_BAD_INPUT, str(e))
            return '', u.HTTP_NO_CONTENT


api.add_resource(UserList, '/api/users')
api.add_resource(CaseList, '/api/cases')
api.add_resource(Login, '/api/login')


api.add_resource(User, '/api/users/<string:user_id>', '/api/users')
api.add_resource(Case, '/api/cases/<string:case_id>', '/api/cases')


def handle_constraints():
    email_index = IndexModel('email', name='email_index', unique=True)
    username_index = IndexModel('user_name', name='user_name_index', unique=True)
    phone_number_index = IndexModel(
        [("phone_number.number", pymongo.ASCENDING), ("phone_number.country_code", pymongo.ASCENDING)],
        name='phone_number_index', unique=True)
    collections = db.collection_names(include_system_collections=False)
    for name in ['users', 'cases']:
        if name not in collections:
            pymongo.collection.Collection(db, name, create=True)
    db.users.create_indexes([email_index, phone_number_index, username_index])


if __name__ == '__main__':
    def _validate_mode(mode):
        if not mode:
            raise argparse.ArgumentTypeError('mode can not be None or empty')
        modes = ['dev', 'production']
        if mode not in modes:
            raise argparse.ArgumentTypeError('mode should be a value in %s', str[modes])
        return mode

    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', dest='mode', required=True, type=_validate_mode,
                        help='The GI server mode (production|dev)')
    args = parser.parse_args()
    mode = args.mode
    auth.mode = mode
    print('---------------------------------------------')
    print('GI server starts under %s mode' % mode)
    print('---------------------------------------------')
    mongo = pymongo.MongoClient(config.get_db_uri())
    db = mongo.get_default_database()
    auth.db = db
    handle_constraints()
    app.run(debug=False)